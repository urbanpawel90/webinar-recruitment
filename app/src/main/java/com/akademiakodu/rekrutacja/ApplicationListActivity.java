package com.akademiakodu.rekrutacja;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.akademiakodu.rekrutacja.adapter.ApplicationsAdapter;
import com.akademiakodu.rekrutacja.api.ApplicationApi;
import com.akademiakodu.rekrutacja.api.ListApplicationsResponse;
import com.akademiakodu.rekrutacja.model.ApplicationListElement;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicationListActivity extends AbstractLogoActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ApplicationsAdapter.OnApplicationEntryClick {
    public static void start(Context context) {
        context.startActivity(new Intent(context, ApplicationListActivity.class));
    }

    @BindView(R.id.application_list)
    protected RecyclerView mApplicationList;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    @BindView(R.id.navigation)
    protected NavigationView mNavigationView;

    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private ApplicationsAdapter mApplicationAdapter;
    private ApplicationApi mApi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);
        ButterKnife.bind(this);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mNavigationView.setNavigationItemSelectedListener(this);

        setupRecyclerView();
        setupApi();
    }

    private void setupApi() {
        mApi = RecruitmentApplication.getApi(this);
        mApi.listApplications().enqueue(new Callback<ListApplicationsResponse>() {
            @Override
            public void onResponse(Call<ListApplicationsResponse> call,
                                   Response<ListApplicationsResponse> response) {
                mApplicationAdapter.setApplicationList(response.body().getApplication());
            }

            @Override
            public void onFailure(Call<ListApplicationsResponse> call, Throwable t) {
                Toast.makeText(ApplicationListActivity.this, "Błąd wczytywania listy !",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        mApplicationAdapter = new ApplicationsAdapter(this);

        mApplicationList.setLayoutManager(new LinearLayoutManager(this));
        mApplicationList.setAdapter(mApplicationAdapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_about) {
            AboutUsActivity.start(this);
            return true;
        }
        return false;
    }

    @Override
    public void onEntryClick(ApplicationListElement element) {
        ApplicationDetailsActivity.start(this, element.getId());
    }
}
