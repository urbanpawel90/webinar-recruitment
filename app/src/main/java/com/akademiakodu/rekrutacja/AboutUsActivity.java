package com.akademiakodu.rekrutacja;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends AbstractLogoActivity {
    public static void start(Context context) {
        context.startActivity(new Intent(context, AboutUsActivity.class));
    }

    @BindView(R.id.webview)
    protected WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);

        mWebView.loadUrl("http://www.akademiakodu.pl/");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
