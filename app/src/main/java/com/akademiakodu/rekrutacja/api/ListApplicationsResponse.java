package com.akademiakodu.rekrutacja.api;

import com.akademiakodu.rekrutacja.model.ApplicationListElement;

import java.util.List;

public class ListApplicationsResponse {
    private List<ApplicationListElement> application;

    public List<ApplicationListElement> getApplication() {
        return application;
    }

    public void setApplication(List<ApplicationListElement> application) {
        this.application = application;
    }
}
