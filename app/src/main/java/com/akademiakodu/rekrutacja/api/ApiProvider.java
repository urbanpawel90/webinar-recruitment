package com.akademiakodu.rekrutacja.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiProvider {
    public static final String BASE_URL = "https://recruitment-api.herokuapp.com";

    private ApiProvider() {
    }

    public static ApplicationApi getApi(Retrofit retrofit) {
        return retrofit.create(ApplicationApi.class);
    }

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();
    }
}
