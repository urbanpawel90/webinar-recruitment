package com.akademiakodu.rekrutacja.api;

import com.akademiakodu.rekrutacja.model.ApplicationDetails;
import com.akademiakodu.rekrutacja.model.Rating;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApplicationApi {
    @GET("applications")
    Call<ListApplicationsResponse> listApplications();

    @GET("applications/{id}")
    Call<ApplicationDetails> applicationDetails(@Path("id") int id);

    @POST("applications/{id}/ratings")
    Call<Void> saveRating(@Path("id") int id, @Body Rating rating);
}
