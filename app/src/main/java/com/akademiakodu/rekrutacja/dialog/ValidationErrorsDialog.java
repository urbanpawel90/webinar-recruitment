package com.akademiakodu.rekrutacja.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.akademiakodu.rekrutacja.model.ValidationError;

public class ValidationErrorsDialog extends AppCompatDialogFragment
        implements DialogInterface.OnClickListener {
    private static final String ARG_ERRORS = "errors";

    private ValidationError mErrors;

    public static void showDialog(FragmentActivity activity, ValidationError errors) {
        ValidationErrorsDialog dialog = new ValidationErrorsDialog();
        Bundle args = new Bundle();

        args.putSerializable(ARG_ERRORS, errors);
        dialog.setArguments(args);

        dialog.show(activity.getSupportFragmentManager(), ValidationErrorsDialog.class.getName());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrors = (ValidationError) getArguments().getSerializable(ARG_ERRORS);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle("Błąd")
                .setMessage(buildDialogMessage())
                .setPositiveButton("OK", this)
                .create();
        return dialog;
    }

    private String buildDialogMessage() {
        StringBuilder dialogMessage = new StringBuilder();
        for (String nameError : mErrors.getName()) {
            dialogMessage.append(nameError).append("\n");
        }
        for (String rateError : mErrors.getRating()) {
            dialogMessage.append(rateError).append("\n");
        }
        // Usuwamy ostatni znak nowej linii
        dialogMessage.delete(dialogMessage.length() - 1, dialogMessage.length());

        return dialogMessage.toString();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.cancel();
    }
}
