package com.akademiakodu.rekrutacja;

import android.app.Application;
import android.content.Context;

import com.akademiakodu.rekrutacja.api.ApiProvider;
import com.akademiakodu.rekrutacja.api.ApplicationApi;

import retrofit2.Retrofit;

public class RecruitmentApplication extends Application {
    private ApplicationApi mApi;
    private Retrofit mRetrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        mRetrofit = ApiProvider.getRetrofit();
        mApi = ApiProvider.getApi(mRetrofit);
    }

    public ApplicationApi getApi() {
        return mApi;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public static ApplicationApi getApi(Context context) {
        RecruitmentApplication application = (RecruitmentApplication) context.getApplicationContext();
        return application.getApi();
    }

    public static Retrofit getRetrofit(Context context) {
        RecruitmentApplication application = (RecruitmentApplication) context.getApplicationContext();
        return application.getRetrofit();
    }
}
