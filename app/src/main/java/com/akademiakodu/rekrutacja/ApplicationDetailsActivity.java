package com.akademiakodu.rekrutacja;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akademiakodu.rekrutacja.adapter.ApplicationGalleryAdapter;
import com.akademiakodu.rekrutacja.adapter.ApplicationRatingsAdapter;
import com.akademiakodu.rekrutacja.api.ApiProvider;
import com.akademiakodu.rekrutacja.api.ApplicationApi;
import com.akademiakodu.rekrutacja.dialog.ValidationErrorsDialog;
import com.akademiakodu.rekrutacja.model.ApplicationDetails;
import com.akademiakodu.rekrutacja.model.Rating;
import com.akademiakodu.rekrutacja.model.ValidationError;
import com.akademiakodu.rekrutacja.widget.StarRatingView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ApplicationDetailsActivity extends AbstractLogoActivity
        implements ApplicationGalleryAdapter.OnGalleryItemClick {
    public static final String EXTRA_ID = "id";

    public static void start(Context context, int id) {
        Intent startIntent = new Intent(context, ApplicationDetailsActivity.class);
        startIntent.putExtra(EXTRA_ID, id);
        context.startActivity(startIntent);
    }

    @BindView(R.id.item_image)
    protected ImageView mIcon;
    @BindView(R.id.item_title)
    protected TextView mTitle;
    @BindView(R.id.description)
    protected TextView mDescription;
    @BindView(R.id.gallery)
    protected RecyclerView mGalleryList;
    @BindView(R.id.google_play)
    protected ImageView mGooglePlay;
    @BindView(R.id.app_store)
    protected ImageView mAppStore;
    @BindView(R.id.rater_name)
    protected EditText mRaterName;
    @BindView(R.id.rating_value)
    protected StarRatingView mRatingValue;
    @BindView(R.id.ratings)
    protected RecyclerView mRatings;

    private int mCurrentId;
    private ApplicationApi mApi;
    private ApplicationDetails mDetails;

    private ApplicationGalleryAdapter mGalleryAdapter;
    private ApplicationRatingsAdapter mRatingsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_details);
        ButterKnife.bind(this);

        mRatingValue.setAllowEdit(true);

        mCurrentId = getIntent().getIntExtra(EXTRA_ID, -1);
        if (mCurrentId == -1) {
            finish();
            return;
        }

        mApi = RecruitmentApplication.getApi(this);
        startDetailsRequest();
        setupGallery();
        setupRatings();
    }

    private void setupRatings() {
        mRatings.setLayoutManager(new LinearLayoutManager(this));
        mRatingsAdapter = new ApplicationRatingsAdapter();
        mRatings.setAdapter(mRatingsAdapter);
    }

    private void setupGallery() {
        mGalleryList.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mGalleryAdapter = new ApplicationGalleryAdapter(ApiProvider.BASE_URL, this);
        mGalleryList.setAdapter(mGalleryAdapter);
    }

    private void startDetailsRequest() {
        mApi.applicationDetails(mCurrentId).enqueue(new Callback<ApplicationDetails>() {
            @Override
            public void onResponse(Call<ApplicationDetails> call, Response<ApplicationDetails> response) {
                mDetails = response.body();
                setValues();
            }

            @Override
            public void onFailure(Call<ApplicationDetails> call, Throwable t) {
                Toast.makeText(ApplicationDetailsActivity.this, "Błąd wczytywania szczegółów !",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void setValues() {
        mTitle.setText(mDetails.getName());
        mDescription.setText(mDetails.getDescription());
        Picasso.with(this).load(ApiProvider.BASE_URL + mDetails.getIcon()).into(mIcon);
        mGalleryAdapter.setImages(mDetails.getGallery());
        mRatingsAdapter.setRatings(mDetails.getRating());
    }

    @OnClick(R.id.btn_rate)
    protected void onRateClick() {
        Rating rating = new Rating();
        rating.setName(mRaterName.getText().toString());
        rating.setRate(mRatingValue.getRating());

        mApi.saveRating(mCurrentId, rating).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                onAddRatingResponse(response);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(ApplicationDetailsActivity.this, "Błąd wysyłania oceny !",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mRaterName.setText("");
        mRatingValue.setRating(0);
    }

    private void onAddRatingResponse(Response<Void> response) {
        if (response.isSuccessful()) {
            startDetailsRequest();
        } else {
            Converter<ResponseBody, ValidationError> bodyConverter =
                    RecruitmentApplication.getRetrofit(this)
                            .responseBodyConverter(ValidationError.class, new Annotation[0]);
            try {
                ValidationError errors = bodyConverter.convert(response.errorBody());
                ValidationErrorsDialog.showDialog(this, errors);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.google_play)
    protected void onGooglePlayClick() {
        if (mDetails.getLinks() != null && mDetails.getLinks().getAndroid() != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mDetails.getLinks().getAndroid())));
        }
    }

    @OnClick(R.id.app_store)
    protected void onAppStoreClick() {
        if (mDetails.getLinks() != null && mDetails.getLinks().getIos() != null) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mDetails.getLinks().getIos())));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
        FullscreenGalleryActivity.start(this, mDetails.getGallery(), position);
    }
}
