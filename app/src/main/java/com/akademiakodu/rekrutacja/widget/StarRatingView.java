package com.akademiakodu.rekrutacja.widget;


import com.akademiakodu.rekrutacja.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StarRatingView extends LinearLayout {
    @BindViews({R.id.star_1, R.id.star_2, R.id.star_3, R.id.star_4, R.id.star_5})
    protected List<ImageView> mStars;

    private int mCurrentRating;
    private boolean mAllowEdit;

    public StarRatingView(Context context) {
        super(context);
        init();
    }

    public StarRatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StarRatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public StarRatingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        LayoutInflater.from(getContext()).inflate(R.layout.widget_rating_stars, this, true);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.star_1, R.id.star_2, R.id.star_3, R.id.star_4, R.id.star_5})
    protected void onStarClick(View star) {
        if (!mAllowEdit) {
            return;
        }
        setRating(mStars.indexOf(star) + 1);
    }

    public void setAllowEdit(boolean allowEdit) {
        mAllowEdit = allowEdit;
    }

    /**
     * @param rating Ocena w skali 1-5
     */
    public void setRating(int rating) {
        mCurrentRating = rating;
        for (int i = 0; i < mStars.size(); i++) {
            mStars.get(i)
                    .setImageResource(i < rating ?
                            R.drawable.ic_star_s :
                            R.drawable.ic_star);
        }
    }

    public int getRating() {
        return mCurrentRating;
    }
}
