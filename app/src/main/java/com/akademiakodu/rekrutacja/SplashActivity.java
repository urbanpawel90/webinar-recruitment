package com.akademiakodu.rekrutacja;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Odkładamy otwarcie następnego Activity o 2 sekundu
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ApplicationListActivity.start(SplashActivity.this);
                finish();
            }
        }, 2000);
    }
}
