package com.akademiakodu.rekrutacja.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.akademiakodu.rekrutacja.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FullscreenGalleryPagerAdapter extends PagerAdapter {
    private List<String> mImages;
    private String mBaseUrl;

    public FullscreenGalleryPagerAdapter(List<String> mImages, String mBaseUrl) {
        this.mImages = mImages;
        this.mBaseUrl = mBaseUrl;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        String imageUrl = mBaseUrl + mImages.get(position);

        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View elementView = inflater.inflate(R.layout.fullscreen_gallery_item, container, false);
        ImageView imageView = (ImageView) elementView.findViewById(R.id.fullscreen_image);

        container.addView(elementView);

        Picasso.with(container.getContext()).load(imageUrl).into(imageView);

        return elementView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
