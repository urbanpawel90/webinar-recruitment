package com.akademiakodu.rekrutacja.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akademiakodu.rekrutacja.R;
import com.akademiakodu.rekrutacja.api.ApiProvider;
import com.akademiakodu.rekrutacja.model.ApplicationListElement;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ApplicationsAdapter extends RecyclerView.Adapter<ApplicationsAdapter.ApplicationViewHolder> {
    private List<ApplicationListElement> mApplicationList;
    private OnApplicationEntryClick mClickListener;

    public ApplicationsAdapter(OnApplicationEntryClick clickListener) {
        mApplicationList = Collections.emptyList();
        mClickListener = clickListener;
    }

    public void setApplicationList(List<ApplicationListElement> appList) {
        mApplicationList = appList;
        notifyDataSetChanged();
    }

    @Override
    public ApplicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.application_list_item, parent, false);
        return new ApplicationViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(ApplicationViewHolder holder, int position) {
        holder.bindView(mApplicationList.get(position));
    }

    @Override
    public int getItemCount() {
        return mApplicationList.size();
    }

    public static class ApplicationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_image)
        protected ImageView mImage;
        @BindView(R.id.item_title)
        protected TextView mTitle;

        private ApplicationListElement mCurrentElement;
        private OnApplicationEntryClick mClickListener;

        public ApplicationViewHolder(View itemView, OnApplicationEntryClick clickListener) {
            super(itemView);
            mClickListener = clickListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick
        protected void onClick() {
            if (mClickListener != null) {
                mClickListener.onEntryClick(mCurrentElement);
            }
        }

        private void bindView(ApplicationListElement element) {
            mCurrentElement = element;
            mTitle.setText(element.getTitle());
            Picasso.with(mImage.getContext())
                    .load(ApiProvider.BASE_URL + element.getImage())
                    .into(mImage);
        }
    }

    public interface OnApplicationEntryClick {
        void onEntryClick(ApplicationListElement element);
    }
}
