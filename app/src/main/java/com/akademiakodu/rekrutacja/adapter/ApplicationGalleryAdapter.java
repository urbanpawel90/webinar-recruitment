package com.akademiakodu.rekrutacja.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.akademiakodu.rekrutacja.R;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ApplicationGalleryAdapter
        extends RecyclerView.Adapter<ApplicationGalleryAdapter.GalleryViewHolder> {
    private List<String> mImages;
    private String mBaseUrl;
    private OnGalleryItemClick mClickListener;

    public ApplicationGalleryAdapter(String mBaseUrl, OnGalleryItemClick clickListener) {
        this.mBaseUrl = mBaseUrl;
        this.mImages = Collections.emptyList();
        this.mClickListener = clickListener;
    }

    public void setImages(List<String> mImages) {
        this.mImages = mImages;
        notifyDataSetChanged();
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.gallery_item, parent, false);
        return new GalleryViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        holder.loadUrl(mBaseUrl + mImages.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public static class GalleryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.gallery_image)
        protected ImageView mImageView;

        private int mPosition;
        private OnGalleryItemClick mClickListener;

        public GalleryViewHolder(View itemView, OnGalleryItemClick clickListener) {
            super(itemView);
            this.mClickListener = clickListener;
            ButterKnife.bind(this, itemView);
        }

        public void loadUrl(String imageUrl, int position) {
            this.mPosition = position;
            Picasso.with(mImageView.getContext()).load(imageUrl).into(mImageView);
        }

        @OnClick
        protected void onImageClick() {
            if (mClickListener != null) {
                mClickListener.onItemClick(mPosition);
            }
        }
    }

    public interface OnGalleryItemClick {
        void onItemClick(int position);
    }
}
