package com.akademiakodu.rekrutacja.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akademiakodu.rekrutacja.R;
import com.akademiakodu.rekrutacja.model.Rating;
import com.akademiakodu.rekrutacja.widget.StarRatingView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApplicationRatingsAdapter extends RecyclerView.Adapter<ApplicationRatingsAdapter.RatingViewHolder> {
    private List<Rating> mRatings = Collections.emptyList();

    public void setRatings(List<Rating> mRatings) {
        this.mRatings = mRatings;
        notifyDataSetChanged();
    }

    @Override
    public RatingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rating_list_item, parent, false);
        return new RatingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RatingViewHolder holder, int position) {
        holder.bind(mRatings.get(position));
    }

    @Override
    public int getItemCount() {
        return mRatings.size();
    }

    public static class RatingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rater_name)
        protected TextView mRater;
        @BindView(R.id.rating_value)
        protected StarRatingView mRateValue;

        public RatingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Rating rating) {
            mRater.setText(rating.getName());
            mRateValue.setRating(rating.getRate());
        }
    }
}
