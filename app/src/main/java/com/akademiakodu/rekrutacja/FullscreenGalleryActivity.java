package com.akademiakodu.rekrutacja;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.akademiakodu.rekrutacja.adapter.FullscreenGalleryPagerAdapter;
import com.akademiakodu.rekrutacja.api.ApiProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FullscreenGalleryActivity extends AbstractLogoActivity {
    private static final String EXTRA_POSITION = "position";
    private static final String EXTRA_IMAGES = "images";

    public static void start(Context context, List<String> images, int position) {
        Intent startIntent = new Intent(context, FullscreenGalleryActivity.class);
        startIntent.putExtra(EXTRA_POSITION, position);
        startIntent.putStringArrayListExtra(EXTRA_IMAGES, new ArrayList<>(images));

        context.startActivity(startIntent);
    }

    @BindView(R.id.gallery_viewpager)
    protected ViewPager mViewPager;

    private List<String> mImages;
    private int mInitialPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_gallery);
        ButterKnife.bind(this);

        mImages = getIntent().getStringArrayListExtra(EXTRA_IMAGES);
        mInitialPosition = getIntent().getIntExtra(EXTRA_POSITION, 0);

        mViewPager.setAdapter(new FullscreenGalleryPagerAdapter(mImages, ApiProvider.BASE_URL));
        mViewPager.setCurrentItem(mInitialPosition);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
