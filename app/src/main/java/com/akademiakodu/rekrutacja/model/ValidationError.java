package com.akademiakodu.rekrutacja.model;


import java.io.Serializable;
import java.util.List;

public class ValidationError implements Serializable {
    private List<String> name;
    private List<String> rating;

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<String> getRating() {
        return rating;
    }

    public void setRating(List<String> rating) {
        this.rating = rating;
    }
}
