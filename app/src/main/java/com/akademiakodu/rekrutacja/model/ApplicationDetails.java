package com.akademiakodu.rekrutacja.model;

import java.util.List;

public class ApplicationDetails {
    private String name;
    private String icon;
    private String description;
    private List<String> gallery;
    private StoreLinks links;
    private List<Rating> rating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public StoreLinks getLinks() {
        return links;
    }

    public void setLinks(StoreLinks links) {
        this.links = links;
    }

    public List<Rating> getRating() {
        return rating;
    }

    public void setRating(List<Rating> rating) {
        this.rating = rating;
    }

    public static class StoreLinks {
        private String android;
        private String ios;

        public String getAndroid() {
            return android;
        }

        public void setAndroid(String android) {
            this.android = android;
        }

        public String getIos() {
            return ios;
        }

        public void setIos(String ios) {
            this.ios = ios;
        }
    }
}
